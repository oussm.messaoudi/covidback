FROM openjdk:8-jre-alpine
EXPOSE 8080
ADD /build/libs/covidback-0.0.1-SNAPSHOT.war covidback.war
ENTRYPOINT ["java","-jar","covidback.war"]
