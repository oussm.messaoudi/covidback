#!/bin/bash

REGISTRY_IMAGE="index.docker.io/nabilberd/covidback:latest"
REGISTRY_USER=$CI_REGISTRY_USER
REGISTRY_PASSWORD=$CI_REGISTRY_PASSWORD
REGISTRY=$CI_REGISTRY

# Authentification to docker hub
docker login -u "${REGISTRY_USER}" -p "${REGISTRY_PASSWORD}" "${REGISTRY}"

# Pull the last version our docker image
docker pull "${REGISTRY_IMAGE}"

# Run the docker image
docker run -d -it -p 8080:8080 --name covidback "${REGISTRY_IMAGE}"
